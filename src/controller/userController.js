const User = require("../models/user");
const { userValidator, option } = require("../validator/userValidator");
const sendToken = require("../../utils/jwtToken");

exports.registerUser = async (req, res, next) => {
    try {
        const { error, value } = userValidator.validate(req.body);
        if (error) {
            res.status(400).json({
                success: false,
                message: `${error.message}`,
            });
        }
        const { firstName, lastName, userName, email, phone, password } =
            req.body;
        const user = await User.create({
            firstName,
            lastName,
            userName,
            email,
            phone,
            password,
        });

        if (user)
            res.status(200).json({
                success: true,
                message: "User created successfully",
            });
    } catch (error) {
        res.json({ error });
    }
};

exports.loginUser = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            res.status(400).json({
                success: false,
                message: `Please Enter Email & Password`,
            });
        }
        const user = await User.findOne({ email }).select("+password");
        if (!user) {
            res.status(401).json({
                success: false,
                message: `Invalid Email or Password`,
            });
        }

        const isPassMatch = await user.comparePassword(password);

        if (!isPassMatch) {
            res.status(401).json({
                success: false,
                message: `Invalid Email or Password`,
            });
        } else {
            sendToken(user, 200, res);
        }
    } catch (error) {
        res.json({ error });
    }
};

exports.logout = async (req, res, next) => {
    try {
        res.cookie("token", null, {
            expires: new Date(Date.now()),
            httpOnly: true,
        });
        res.status(200).json({
            success: true,
            message: "User logged out",
        });
    } catch (error) {
        res.json({ error });
    }
};
