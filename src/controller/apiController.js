const User = require("../models/user");
const Assessment = require("../models/assessment");
const { assessmentValidator } = require("../validator/assessmentValidator");
const { submissionValidator } = require("../validator/submissionValidator");
const Submission = require("../models/submission");
const { gradeValidator } = require("../validator/gradeValidatior");
const Grade = require("../models/grade");

exports.updateUser = async (req, res, next) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: true,
        });
        res.status(200).json({
            success: true,
            message: "User updated successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.deleteUser = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            res.status(404).json({
                success: false,
                message: `User does not exist with id: ${req.params.id}`,
            });
        }

        await user.remove();
        res.status(200).json({
            success: true,
            message: "User deleted successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

// assessment controller starts here
exports.addAssessment = async (req, res, next) => {
    try {
        const { error, value } = assessmentValidator.validate(req.body);
        if (error) {
            res.status(400).json({
                success: false,
                message: `${error.message}`,
            });
        }
        const { mentor } = req.body;
        const user = await User.findById(mentor);
        if (!user)
            res.status(404).json({
                success: false,
                message: `User not found`,
            });
        else if (user.userType != "Mentor")
            res.status(400).json({
                success: false,
                message: `You Can't add this user as a Mentor`,
            });

        const assessment = await Assessment.create(req.body);
        if (!assessment)
            res.status(400).json({
                success: false,
                message: `Can't create a new assessment`,
            });

        res.status(200).json({
            success: true,
            message: "New assessment added successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.updateAssessment = async (req, res, next) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: true,
        });
        res.status(200).json({
            success: true,
            message: "User updated successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.deleteAssessment = async (req, res, next) => {
    try {
        const assessmant = await Assessment.findById(req.params.id);
        if (!assessmant) {
            res.status(400).json({
                success: false,
                message: `Assessment does not exist with id: ${req.params.id}`,
            });
        }

        await assessmant.remove();
        res.status(200).json({
            success: true,
            message: "Assessment deleted successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

// assessment controller end

// submission controller start
exports.addSubmission = async (req, res, next) => {
    try {
        // data validation check
        const { error, value } = submissionValidator.validate(req.body);
        if (error) {
            res.status(400).json({
                success: false,
                message: `${error.message}`,
            });
        }
        const { assessment, student } = req.body;
        const asses = await Assessment.findById(assessment);
        if (!asses)
            res.status(400).json({
                success: false,
                message: `Assessment not found`,
            }); // if assessment was not found then return here
        const user = await User.findById(student);
        if (!user)
            res.status(400).json({
                success: false,
                message: `Student not found`,
            }); // if user was not found then return here
        if (user.userType !== "Student")
            // if user type is not Student then return here
            res.status(400).json({
                success: false,
                message: `This user can't submit the assessment`,
            });

        const submissionCk = await Submission.findOne({ student, assessment });
        if (submissionCk)
            // if you already have a submission of this assessment then return here
            res.status(400).json({
                success: false,
                message: `Can't submit the assessment twice`,
            });

        asses.allSubmissions.push(student); // submission id added to the assessment schema
        await asses.save();
        const submission = await Submission.create(req.body);
        if (!submission)
            res.status(400).json({
                success: false,
                message: `Can't submit the assessment`,
            });

        res.status(200).json({
            success: true,
            message: "Assessment submission successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.updateSubmission = async (req, res, next) => {
    try {
        const user = await Submission.findByIdAndUpdate(
            req.params.id,
            req.body,
            {
                new: true,
                runValidators: true,
                useFindAndModify: true,
            }
        );
        res.status(200).json({
            success: true,
            message: "Submission updated successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.deleteSubmission = async (req, res, next) => {
    try {
        const submission = await Submission.findById(req.params.id);
        if (!submission) {
            // checks the submission does exist or not

            res.status(400).json({
                success: false,
                message: `Submission does not exist with id: ${req.params.id}`,
            });
        }

        await submission.remove();
        res.status(200).json({
            success: true,
            message: "Submission deleted successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

//submission controller end

//grade controller start
exports.addGrade = async (req, res, next) => {
    try {
        const { error, value } = gradeValidator.validate(req.body);
        if (error) {
            res.status(400).json({
                success: false,
                message: `${error.message}`,
            });
        }

        const { assessment } = req.body;
        const assess = await Assessment.findById(assessment);
        if (!assess)
            res.status(400).json({
                success: false,
                message: `Assessment not found`,
            });
        // if assessment not found then return here
        const grade = await Grade.create(req.body);
        if (!grade)
            res.status(400).json({
                success: false,
                message: `Can't submit the Grade`,
            });
        res.status(200).json({
            success: true,
            message: "Assessment submission successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.updateGrade = async (req, res, next) => {
    try {
        const grade = await Grade.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
            useFindAndModify: true,
        });
        res.status(200).json({
            success: true,
            message: "Grade updated successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};

exports.deleteGrade = async (req, res, next) => {
    try {
        const grade = await Grade.findById(req.params.id);
        if (!grade) {
            // checks the grade does exist or not
            res.status(400).json({
                success: false,
                message: `Grade does not exist with id: ${req.params.id}`,
            });
        }

        await grade.remove();
        res.status(200).json({
            success: true,
            message: "Grade deleted successfully",
        });
    } catch (error) {
        res.json({ error });
    }
};
// grade controller end

exports.getSubmissions = async (req, res, next) => {
    try {
        const id = req.user._id;
        if (req.user.userType === "Student") {
            const submissions = await Submission.find({ student: id });

            if (!submissions)
                res.status(404).json({
                    success: false,
                    message: `You don't have any submissions yet!`,
                });

            res.status(200).json({
                count: submissions.length,
                success: true,
                submissions,
            });
        } else {
            const submissions = await Submission.find();
            if (!submissions)
                res.status(404).json({
                    success: false,
                    message: `No one submit a single assessment`,
                });

            res.status(200).json({
                count: submissions.length,
                success: true,
                submissions,
            });
        }
    } catch (error) {
        res.json({ error });
    }
};
