const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const gradeSchema = new Schema(
    {
        assessment: {
            type: mongoose.Schema.ObjectId,
            ref: "Assessment",
            required: true,
        },
        mark: {
            type: Number,
            required: true,
        },
        remark: {
            type: Number,
        },
    },
    { timestamps: true }
);

const Grade = mongoose.model("grades", gradeSchema);
module.exports = Grade;
