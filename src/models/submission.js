const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const submissionSchema = new Schema(
    {
        student: {
            type: mongoose.Schema.ObjectId,
            ref: "User",
            required: true,
            unique: true,
        },
        assessment: {
            type: mongoose.Schema.ObjectId,
            ref: "Assessment",
            required: true,
        },
        dateOfSubmission: {
            type: Date,
            required: true,
        },
        grade: {
            type: mongoose.Schema.ObjectId,
            ref: "Grade",
        },
        links: [{ type: String, required: true }],
    },
    { timestamps: true }
);

const Submission = mongoose.model("submissions", submissionSchema);
module.exports = Submission;
