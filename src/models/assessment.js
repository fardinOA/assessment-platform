const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const assessmentSchema = new Schema(
    {
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        deadline: {
            type: Date,
            required: true,
        },
        mentor: {
            type: mongoose.Schema.ObjectId,
            ref: "User",
            required: true,
        },
        allSubmissions: [{ type: mongoose.Schema.ObjectId, ref: "User" }],
    },
    { timestamps: true }
);

const Assessment = mongoose.model("assessments", assessmentSchema);
module.exports = Assessment;
