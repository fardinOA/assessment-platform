const Joi = require("joi");
const userValidator = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string(),
    userName: Joi.string().required().alphanum().min(4).max(15),
    userType: Joi.string(),

    email: Joi.string().trim().required().email(),
    phone: Joi.string().trim().required(),
    password: Joi.string()
        .required()
        .pattern(new RegExp("^[a-zA-Z0-9]{6,30}$")),
});

const option = {
    abrotEarly: false,
    allowUnknown: true,
    srtipUnknown: true,
};

module.exports = {
    userValidator,

    option,
};
