const Joi = require("joi");
const assessmentValidator = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().required(),
    deadline: Joi.date().required(),
    mentor: Joi.string().hex().length(24),
});

const option = {
    abrotEarly: false,
    allowUnknown: true,
    srtipUnknown: true,
};

module.exports = {
    assessmentValidator,

    option,
};
