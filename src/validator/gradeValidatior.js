const Joi = require("joi");
const gradeValidator = Joi.object({
    assessment: Joi.string().hex().length(24).required(),
    mark: Joi.number().required(),
});

const option = {
    abrotEarly: false,
    allowUnknown: true,
    srtipUnknown: true,
};

module.exports = {
    gradeValidator,
    option,
};
