const Joi = require("joi");
const submissionValidator = Joi.object({
    student: Joi.string().hex().length(24).required(),
    assessment: Joi.string().hex().length(24).required(),
    dateOfSubmission: Joi.date().required(),
    links: [Joi.string().required()],
});

const option = {
    abrotEarly: false,
    allowUnknown: true,
    srtipUnknown: true,
};

module.exports = {
    submissionValidator,
    option,
};
