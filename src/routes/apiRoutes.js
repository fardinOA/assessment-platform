const express = require("express");
const { isAuth, authRole } = require("../../middleware/auth");
const {
    updateUser,
    deleteUser,
    addAssessment,
    updateAssessment,
    deleteAssessment,
    addSubmission,
    updateSubmission,
    deleteSubmission,
    addGrade,
    updateGrade,
    deleteGrade,
    getSubmissions,
} = require("../controller/apiController");
const router = express.Router();
router.put("/admin/update-user/:id", isAuth, authRole("Admin"), updateUser);
router.delete("/admin/delete-user/:id", isAuth, authRole("Admin"), deleteUser);

// assessment routes
router.post(
    "/admin/add-assessment",
    isAuth,
    authRole("Admin", "Mentor"),
    addAssessment
);
router.put(
    "/admin/update-assessment/:id",
    isAuth,
    authRole("Admin"),
    updateAssessment
);

router.delete(
    "/admin/delete-assessment/:id",
    isAuth,
    authRole("Admin"),
    deleteAssessment
);
// submission routes
router.post(
    "/admin/add-submission",
    isAuth,
    authRole("Admin", "Student"),
    addSubmission
);

router.put(
    "/admin/update-submission/:id",
    isAuth,
    authRole("Admin"),
    updateSubmission
);

router.delete(
    "/admin/delete-submission/:id",
    isAuth,
    authRole("Admin"),
    deleteSubmission
);

// grader routes
router.post("/admin/add-grade", isAuth, authRole("Admin", "Mentor"), addGrade);

router.put("/admin/update-grade/:id", isAuth, authRole("Admin"), updateGrade);

router.delete(
    "/admin/delete-grade/:id",
    isAuth,
    authRole("Admin"),
    deleteGrade
);

router.get(
    "/get-submission",
    isAuth,
    authRole("Admin", "Mentor", "Student"),
    getSubmissions
);
module.exports = router;
