const jwt = require("jsonwebtoken");
const User = require("../src/models/user");
const isAuth = async (req, res, next) => {
    try {
        const { token } = req.cookies;

        if (!token) {
            res.status(401).json({
                success: false,
                message: `Please login to access this resource`,
            });
        }

        const decodedData = jwt.verify(token, process.env.JWT_SECRET);

        req.user = await User.findById(decodedData.id);

        next();
    } catch (error) {
        res.json({ error });
    }
};

const authRole = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.userType)) {
            res.status(401).json({
                success: false,
                message: `Role: ${req.user.userType} is not allowed to access this resource`,
            });
        }
        next();
    };
};

module.exports = { isAuth, authRole };
